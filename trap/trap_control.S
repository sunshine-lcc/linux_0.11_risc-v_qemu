# save all General-Purpose(GP) registers to context
# struct context *base = &ctx_task;
# base->ra = ra;
# ......

#define REGSIZE 8

.macro reg_save base
	sd ra, 0 * REGSIZE * REGSIZE(\base)
	sd sp, 1 * REGSIZE(\base)
	sd gp, 2 * REGSIZE(\base)
	sd tp, 3 * REGSIZE(\base)
	sd t0, 4 * REGSIZE(\base)
	sd t1, 5 * REGSIZE(\base)
	sd t2, 6 * REGSIZE(\base)
	sd s0, 7 * REGSIZE(\base)
	sd s1, 8 * REGSIZE(\base)
	sd a0, 9 * REGSIZE(\base)
	sd a1, 10 * REGSIZE(\base)
	sd a2, 11 * REGSIZE(\base)
	sd a3, 12 * REGSIZE(\base)
	sd a4, 13 * REGSIZE(\base)
	sd a5, 14 * REGSIZE(\base)
	sd a6, 15 * REGSIZE(\base)
	sd a7, 16 * REGSIZE(\base)
	sd s2, 17 * REGSIZE(\base)
	sd s3, 18 * REGSIZE(\base)
	sd s4, 19 * REGSIZE(\base)
	sd s5, 20 * REGSIZE(\base)
	sd s6, 21 * REGSIZE(\base)
	sd s7, 22 * REGSIZE(\base)
	sd s8, 23 * REGSIZE(\base)
	sd s9, 24 * REGSIZE(\base)
	sd s10, 25 * REGSIZE(\base)
	sd s11, 26 * REGSIZE(\base)
	sd t3, 27 * REGSIZE(\base)
	sd t4, 28 * REGSIZE(\base)
	sd t5, 29 * REGSIZE(\base)
	sd t6, 30 * REGSIZE(\base)
.endm

# restore all General-Purpose(GP) registers from the context
# struct context *base = &ctx_task;
# ra = base->ra;
# ......
.macro reg_restore base
	ld ra, 0 * REGSIZE(\base)
	ld sp, 1 * REGSIZE(\base)
	ld gp, 2 * REGSIZE(\base)
	ld tp, 3 * REGSIZE(\base)
	ld t0, 4 * REGSIZE(\base)
	ld t1, 5 * REGSIZE(\base)
	ld t2, 6 * REGSIZE(\base)
	ld s0, 7 * REGSIZE(\base)
	ld s1, 8 * REGSIZE(\base)
	ld a0, 9 * REGSIZE(\base)
	ld a1, 10 * REGSIZE(\base)
	ld a2, 11 * REGSIZE(\base)
	ld a3, 12 * REGSIZE(\base)
	ld a4, 13 * REGSIZE(\base)
	ld a5, 14 * REGSIZE(\base)
	ld a6, 15 * REGSIZE(\base)
	ld a7, 16 * REGSIZE(\base)
	ld s2, 17 * REGSIZE(\base)
	ld s3, 18 * REGSIZE(\base)
	ld s4, 19 * REGSIZE(\base)
	ld s5, 20 * REGSIZE(\base)
	ld s6, 21 * REGSIZE(\base)
	ld s7, 22 * REGSIZE(\base)
	ld s8, 23 * REGSIZE(\base)
	ld s9, 24 * REGSIZE(\base)
	ld s10, 25 * REGSIZE(\base)
	ld s11, 26 * REGSIZE(\base)
	ld t3, 27 * REGSIZE(\base)
	ld t4, 28 * REGSIZE(\base)
	ld t5, 29 * REGSIZE(\base)
	ld t6, 30 * REGSIZE(\base)
.endm

# Something to note about save/restore:
# - We use sscratch to hold a pointer to context of previous task
# - We use t6 as the 'base' for reg_save/reg_restore, because it is the
#   very bottom register (x31) and would not be overwritten during loading.

.text

	.section trampsec
.globl trampoline
trampoline:
# interrupts and exceptions while in machine mode come here.
.globl trap_vector
# the trap vector base address must always be aligned on a 4-byte boundary
.align 4
trap_vector:
	# save context(registers).

	# csrrw	t6, sscratch, t6	# swap t6 and sscratch
	# reg_save t6
	# csrw	sscratch, t6

	# changed by luchangcheng, save from sp
	add sp, sp, -66 * REGSIZE
	reg_save sp

	# save sepc to context of current task
	csrr	a0, sepc

	//sd	a0, 124(t6)
	sd a0, 31 * REGSIZE(sp)

	# call the C trap handler in trap.c
	csrr	a1, scause
	//csrr	a2, sscratch

	#use sp to store register
	mv a2, sp           

	call	trap_handler

	# trap_handler will return the return address via a0.
	csrw	sepc, a0

	# restore context(registers).
	# csrr	t6, sscratch
	
	reg_restore sp

	# return to whatever we were doing before trap.
	sret

.end