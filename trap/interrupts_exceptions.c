#include "trap.h"
#include "linux/sched.h"
#include "plic.h"
#include "unistd.h"
#include "signal.h"

bool trap_from_interrupt;

extern void panic(const char * s);
extern void trap_vector(void);
extern int64_t system_call(uint64_t a7, uint64_t a0, uint64_t a1, uint64_t a2);
int devintr();
void trap_frame_dump(struct context *tf);
extern struct task_struct *current;
extern fn_ptr sys_call_table[];
void save_trap_info(struct context *ctx);

reg_t trap_handler(reg_t epc, reg_t cause, struct context *cxt)
{
	reg_t return_pc = epc;
	reg_t cause_code = cause & 0xfff;
	reg_t status = r_sstatus();
	save_trap_info(cxt);
	
	if ((r_sstatus() & SSTATUS_SPP) == 0)
	{
		if (cause_code == 8)
		{
			trap_from_interrupt = 0;
			switch (cause_code)
			{
				case 8:
					syslog_print("System call from U-mode!\n");
					return_pc += 4;
					intr_on();
					do_syscall(cxt);
					break;
				default:
					panic("OOPS! What can I do!");
					break;
			}
		}
		else if (devintr() != 0)
		{

		}
		else
		{
			syslog_error("trap_handler", "unexpected scause %p pid=%d\n", r_scause(), current->pid);
    		syslog_error("trap_handler","            sepc=%p stval=%p\n", r_sepc(), r_stval());
    		trap_frame_dump(cxt);
			panic("usertrap");
		}

		unsigned long x = r_sstatus();
  		x &= ~SSTATUS_SPP; // clear SPP to 0 for user mode
  		x |= SSTATUS_SPIE; // enable interrupts in user mode
  		w_sstatus(x);

  		// set S Exception Program Counter to the saved user pc.
  		w_sepc(current->tss.epc);
	}
	else
	{
		//syslog_debug("trap_handler", "trap from supervisor mode!");
		if(intr_get() != 0)
    		panic("kerneltrap: interrupts enabled");

  		if(devintr() == 0){
    		syslog_error("trap_handler", "scause %p", cause);
    		syslog_error("trap_handler", "sepc=%p stval=%p hart=%#x", r_sepc(), r_stval(), r_tp());
    		panic("kerneltrap");
  		}

		w_sepc(epc);
  		w_sstatus(status);
	}


	return return_pc;
}


void do_syscall(struct context *cxt)
{
	syslog_debug("do_syscall", "starting to run %d th system call...", cxt->a7);
	
	if (sys_call_table[cxt->a7] != NULL)
	{
		sys_call_table[cxt->a7](cxt->a0, cxt->a1, cxt->a2);
		if (current->state != TASK_RUNNING || (current->state == TASK_RUNNING && current->counter <= 0))
			schedule();
		do_signal(current->signal);
	}
	else
	{
		syslog_error("do_syscall", "illegal syscall number %d", cxt->a7);
		panic("");
	}

	syslog_debug("do_syscall", "finish %d th system call successfully!", cxt->a7);
}

int devintr() 
{
	trap_from_interrupt = 1;
	reg_t scause = r_scause();
	if ((0x8000000000000000L & scause) && 9 == (scause & 0xff))
	{
		int irq = plic_claim();
		if (UART_IRQ == irq)
		{
			syslog_debug("devintr", "UART_IRQ");
			//int c = sbi_console_getchar();
			//if (-1 != c) {
			//	consoleintr(c);
			//}
		}
		else if (DISK_IRQ == irq)
		{
			syslog_debug("devintr", "DISK_IRQ");
			//disk_intr();
		}
		else if (irq)
		{
			syslog_debug("devintr", "unexpected interrupt irq = %d\n", irq);
		}

		if (irq) { plic_complete(irq);}

		return 1;
	}
	else if (0x8000000000000005L == scause) {
		//timer interrupt
		timer_interrupt(NULL);
		return 2;
	}
	else { return 0;}
}

void trap_frame_dump(struct context *tf)
{
  syslog_error("", "a0: %p\t", tf->a0);
  syslog_error("", "a1: %p\n", tf->a1);
  syslog_error("", "a2: %p\t", tf->a2);
  syslog_error("", "a3: %p\n", tf->a3);
  syslog_error("", "a4: %p\t", tf->a4);
  syslog_error("", "a5: %p\n", tf->a5);
  syslog_error("", "a6: %p\t", tf->a6);
  syslog_error("", "a7: %p\n", tf->a7);
  syslog_error("", "t0: %p\t", tf->t0);
  syslog_error("", "t1: %p\n", tf->t1);
  syslog_error("", "t2: %p\t", tf->t2);
  syslog_error("", "t3: %p\n", tf->t3);
  syslog_error("", "t4: %p\t", tf->t4);
  syslog_error("", "t5: %p\n", tf->t5);
  syslog_error("", "t6: %p\t", tf->t6);
  syslog_error("", "s0: %p\n", tf->s0);
  syslog_error("", "s1: %p\t", tf->s1);
  syslog_error("", "s2: %p\n", tf->s2);
  syslog_error("", "s3: %p\t", tf->s3);
  syslog_error("", "s4: %p\n", tf->s4);
  syslog_error("", "s5: %p\t", tf->s5);
  syslog_error("", "s6: %p\n", tf->s6);
  syslog_error("", "s7: %p\t", tf->s7);
  syslog_error("", "s8: %p\n", tf->s8);
  syslog_error("", "s9: %p\t", tf->s9);
  syslog_error("", "s10: %p\n", tf->s10);
  syslog_error("", "s11: %p\t", tf->s11);
  syslog_error("", "ra: %p\n", tf->ra);
  syslog_error("", "sp: %p\t", tf->sp);
  syslog_error("", "gp: %p\n", tf->gp);
  syslog_error("", "tp: %p\t", tf->tp);
  syslog_error("", "epc: %p\n", tf->epc);
}

void save_trap_info(struct context *ctx)
{
	current->tss.regs[1] = ctx->ra;
	current->tss.regs[2] = ctx->sp;
	current->tss.regs[3] = ctx->gp;
	current->tss.regs[4] = ctx->tp;
	current->tss.regs[5] = ctx->t0;
	current->tss.regs[6] = ctx->t1;
	current->tss.regs[7] = ctx->t2;
	current->tss.regs[8] = ctx->s0;
	current->tss.regs[9] = ctx->s1;
	current->tss.regs[10] = ctx->a0;
	current->tss.regs[11] = ctx->a1;
	current->tss.regs[12] = ctx->a2;
	current->tss.regs[13] = ctx->a3;
	current->tss.regs[14] = ctx->a4;
	current->tss.regs[15] = ctx->a5;
	current->tss.regs[16] = ctx->a6;
	current->tss.regs[17] = ctx->a7;
	current->tss.regs[18] = ctx->s2;
	current->tss.regs[19] = ctx->s3;
	current->tss.regs[20] = ctx->s4;
	current->tss.regs[21] = ctx->s5;
	current->tss.regs[22] = ctx->s6;
	current->tss.regs[23] = ctx->s7;
	current->tss.regs[24] = ctx->s8;
	current->tss.regs[25] = ctx->s9;
	current->tss.regs[26] = ctx->s10;
	current->tss.regs[27] = ctx->s11;
	current->tss.regs[28] = ctx->t3;
	current->tss.regs[29] = ctx->t4;
	current->tss.regs[30] = ctx->t5;
	current->tss.regs[31] = ctx->t6;
	current->tss.epc = ctx->epc;
}