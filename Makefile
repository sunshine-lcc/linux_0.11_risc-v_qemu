mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
ROOTPATH := $(dir $(mkfile_path))

CC = riscv64-unknown-elf-gcc
CFLAGS = -Wall -O -fstrength-reduce  -nostdinc -fomit-frame-pointer -I include
CFLAGS += -mcmodel=medany -mabi=lp64f -march=rv64imafc -fno-common -ffunction-sections -fdata-sections -fstrict-volatile-bitfields
CFLAGS += -fno-zero-initialized-in-bss -ffast-math -fno-math-errno -fsingle-precision-constant -Og -ggdb -Wall
CFLAGS += -Wno-multichar -Wextra -Werror=frame-larger-than=32768 -Wno-unused-parameter -Wno-sign-compare -Wno-error=missing-braces
CFLAGS += -Wno-error=return-type -Wno-error=pointer-sign -Wno-missing-braces -Wno-strict-aliasing -Wno-implicit-fallthrough -Wno-missing-field-initializers
CFLAGS += -Wno-error=comment -Wno-error=logical-not-parentheses -Wno-error=duplicate-decl-specifier -Wno-error=parentheses
CFLAGS += -nostartfiles
CFLAGS += -static
CFLAGS += -I $(ROOTPATH)/include -I $(ROOTPATH)/bsp/include -I $(ROOTPATH)/drivers/include -I $(ROOTPATH)/riscvfunc/include -I $(ROOTPATH)/trap/include
CFLAGS += -DDEBUG

LCFLAGS = -Wl,--gc-sections
LCFLAGS += -Wl,-static
LCFLAGS += -Wl,-EL
LCFLAGS += -Wl,--no-relax
LCFLAGS += -Wl,-T linker.ld
LCFLAGS += -Wl,-M

CFLAGS_USER = -Wall -O -fstrength-reduce -fomit-frame-pointer -I include
CFLAGS_USER  += -mcmodel=medany -mabi=lp64f -march=rv64imafc -fno-common -ffunction-sections -fdata-sections -fstrict-volatile-bitfields
CFLAGS_USER  += -fno-zero-initialized-in-bss -ffast-math -fno-math-errno -fsingle-precision-constant -Os -ggdb -Wall
CFLAGS_USER  += -Wno-multichar -Wextra -Werror=frame-larger-than=32768 -Wno-unused-parameter -Wno-sign-compare -Wno-error=missing-braces
CFLAGS_USER  += -Wno-error=return-type -Wno-error=pointer-sign -Wno-missing-braces -Wno-strict-aliasing -Wno-implicit-fallthrough -Wno-missing-field-initializers
CFLAGS_USER  += -Wno-error=comment -Wno-error=logical-not-parentheses -Wno-error=duplicate-decl-specifier -Wno-error=parentheses
CFLAGS_USER  += -static

LCFLAGS_USER = -Wl,--gc-sections
LCFLAGS_USER += -Wl,-static
LCFLAGS_USER += -Wl,-EL
LCFLAGS_USER += -Wl,--no-relax
LCFLAGS_USER += -Wl,-T linker_user.ld
LCFLAGS_USER += -Wl,-M
LD = riscv64-unknown-elf-ld
LDFLAGS	= -s -x -nostartfiles -static --gc-sections -static -EL --no-relax -T linker.ld
AS = riscv64-unknown-elf-as
OBJCOPY = riscv64-unknown-elf-objcopy
OBJDUMP = riscv64-unknown-elf-objdump
INCLUDEFILE = ./include/*.h ./bsp/include/*.h ./drivers/include/*.h ./riscvfunc/include/*.h ./trap/include/*.h
CSOURCEFILE = $(wildcard *.c ./bsp/*.c ./drivers/*.c ./riscvfunc/*.c ./startup/*.c ./trap/*.c ./kernel/*.c ./kernel/blk_dev/*.c ./kernel/chr_dev/*.c ./fs/*.c ./user/test/*.c)
SSOURCEFILE = $(patsubst %.c,%.S,$(CSOURCEFILE))
SOURCEFILE = $(CSOURCEFILE) $(SSOURCEFILE)
COBJFILE = $(patsubst %.c,%.o,$(CSOURCEFILE))
SOBJFILE = $(patsubst %.S,%.o,$(SSOURCEFILE))

QEMU = qemu-system-riscv64


QEMUOPT = -M virt -m 256M -nographic -smp 1
QEMUOPT += -bios rustsbi-qemu
QEMUOPT += -kernel system.bin
QEMUOPT += -drive file=system.bin,if=none,format=raw,id=x0 # TODO: use true disk instead of kernel
QEMUOPT += -device virtio-blk-device,drive=x0,bus=virtio-mmio-bus.0
QEMUOPT += -append "root=/dev/vda rw console=ttyS0"


all: system.bin system.txt Makefile

image.bin: system.bin system.txt rootfs.bin Makefile
	touch image.bin
	dd if=system.bin of=image.bin
	dd if=rootfs.bin of=image.bin bs=1024 seek=500
	
rootfs.bin: Makefile
	touch rootfs.bin
	sudo dd if=/dev/zero of=rootfs.bin bs=1024 count=360
	sudo mkfs.minix -n14 rootfs.bin 360
	mkdir mnt
	sudo mount -o loop -t minix rootfs.bin mnt
	sudo mkdir mnt/dev
	sudo mkdir mnt/bin
	sudo cp test.aout mnt/bin/sh
	sudo chmod 777 mnt/bin/sh
	sudo df -h mnt
	sudo umount mnt

system.txt: system.elf
	$(OBJDUMP) -d $< > $@

system.bin: system.elf
	$(OBJCOPY) -O binary $< $@

system.elf: ./root_dir.o ./bsp/bsp.o ./drivers/drivers.o ./riscvfunc/riscvfunc.o ./startup/startup.o ./trap/trap.o ./kernel/kernel.o ./mm/mm.o ./fs/fs.o ./user/test/test.o
	$(CC) $(CFLAGS) $(LCFLAGS) -o $@ $^ ./libraries/libm.a ./libraries/libatomic.a ./libraries/libsim.a

./user/test/test.bin: ./user/test/test.elf
	$(OBJCOPY) -O binary $< $@

./user/test/test.elf: ./user/test/test_main.c
	$(CC) $(CFLAGS_USER) $(LCFLAGS_USER) -o $@ $^ ./libraries/libm.a ./libraries/libatomic.a ./libraries/libsim.a
	$(OBJDUMP) -d ./user/test/test.elf  > ./user/test/test.txt

./user/test/test.o: $(patsubst %.S,%.o,$(patsubst %.c,%.o,$(wildcard ./user/test/*.c ./user/test/*.S)))
	$(LD) -r -o $@ $^
	
./root_dir.o: $(patsubst %.S,%.o,$(patsubst %.c,%.o,$(wildcard ./*.c ./*.S)))
	$(LD) -r -o $@ $^

./bsp/bsp.o: $(patsubst %.S,%.o,$(patsubst %.c,%.o,$(wildcard ./bsp/*.c ./bsp/*.S)))
	$(LD) -r -o $@ $^

./drivers/drivers.o: $(patsubst %.S,%.o,$(patsubst %.c,%.o,$(wildcard ./drivers/*.c ./drivers/*.S)))
	$(LD) -r -o $@ $^

./riscvfunc/riscvfunc.o: $(patsubst %.S,%.o,$(patsubst %.c,%.o,$(wildcard ./riscvfunc/*.c ./riscvfunc/*.S)))
	$(LD) -r -o $@ $^

./startup/startup.o: $(patsubst %.S,%.o,$(patsubst %.c,%.o,$(wildcard ./startup/*.c ./startup/*.S)))
	$(LD) -r -o $@ $^

./trap/trap.o: $(patsubst %.S,%.o,$(patsubst %.c,%.o,$(wildcard ./trap/*.c ./trap/*.S)))
	$(LD) -r -o $@ $^

./kernel/kernel.o: $(patsubst %.S,%.o,$(patsubst %.c,%.o,$(wildcard ./kernel/*.c ./kernel/*.S ./kernel/blk_dev/*.c ./kernel/blk_dev/*.S ./kernel/*.S ./kernel/chr_dev/*.c ./kernel/chr_dev/*.S)))
	$(LD) -r -o $@ $^

./mm/mm.o: $(patsubst %.S,%.o,$(patsubst %.c,%.o,$(wildcard ./mm/*.c ./mm/*.S)))
	$(LD) -r -o $@ $^

./fs/fs.o: $(patsubst %.S,%.o,$(patsubst %.c,%.o,$(wildcard ./fs/*.c ./fs/*.S)))
	$(LD) -r -o $@ $^

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<

%.o: %.S
	$(CC) $(CFLAGS) -o $@ -c $<

clean:
	rm -rf *.o ./fs/*.o ./bsp/*.o ./drivers/*.o ./riscvfunc/*.o ./startup/*.o ./trap/*.o ./kernel/*.o ./kernel/blk_dev/*.o ./kernel/chr_dev/*.o ./mm/*.o system.* image.bin rootfs.bin mnt dev bin ./user/test/*.o 

run: all
	$(QEMU) $(QEMUOPT)

debug: all
	$(QEMU) $(QEMUOPT) -S -s

gdb: all
	gdb-multiarch system.elf
	