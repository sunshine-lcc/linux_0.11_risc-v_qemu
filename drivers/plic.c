
#include "types.h"
#include "param.h"
#include "memlayout.h"
#include "riscv.h"
#include "defs.h"
#include "sbi.h"
#include "plic.h"

//
// the riscv Platform Level Interrupt Controller (PLIC).
//


void plic_init(void) {
	writed(1, PLIC + DISK_IRQ * sizeof(uint32));
	writed(1, PLIC + UART_IRQ * sizeof(uint32));

	plic_inithart();
  syslog_debug("plic_init", "plic init done!");
}

void
plic_inithart(void)
{
  int hart = 0;

  // set uart's enable bit for this hart's S-mode. 
  *(uint32*)PLIC_SENABLE(hart)= (1 << UART_IRQ) | (1 << DISK_IRQ);
  // set this hart's S-mode priority threshold to 0.
  *(uint32*)PLIC_SPRIORITY(hart) = 0;
  
}

// ask the PLIC what interrupt we should serve.
int
plic_claim(void)
{
  int hart = 0;
  int irq;

  irq = *(uint32*)PLIC_SCLAIM(hart);

  return irq;
}

// tell the PLIC we've served this IRQ.
void
plic_complete(int irq)
{
  int hart = 0;
  *(uint32*)PLIC_SCLAIM(hart) = irq;
  
}
