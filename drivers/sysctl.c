/* Copyright 2018 Canaan Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "common.h"
#include "bsp.h"
#include "encoding.h"
#include "sysctl.h"

#define SYSCTRL_CLOCK_FREQ_IN0 (26000000UL)

static uint32_t cpu_freq = 390000000;

uint32_t sysctl_cpu_get_freq(void)
{
    return cpu_freq;
}

void sysctl_enable_irq(void)
{
    //set_csr(mie, MIP_MEIP);
    //set_csr(mstatus, MSTATUS_MIE);

    //changed by luchangcheng, convert to supervisor mode registers
    set_csr(sie, SIE_SSIE);
    set_csr(sstatus, SSTATUS_SIE);
}

void sysctl_disable_irq(void)
{
    //clear_csr(mie, MIP_MEIP);
    //clear_csr(mstatus, MSTATUS_MIE);

    //changed by luchangcheng, convert to supervisor mode registers
    clear_csr(sie, SIE_SSIE);
    clear_csr(sstatus, SSTATUS_SIE);
}
