#include "common.h"
#include "drivers.h"
#include "entry.h"
#include "riscv.h"
#include "plic.h"
#include "spinlock.h"
#include "sbi.h"

#define INTERVAL (390000000 / 200) // timer interrupt interval

extern void trap_vector();

extern volatile uint64_t g_wake_up[2];
void kernelvec();
core_instance_t core1_instance;
struct spinlock tickslock;

extern int supervisor_main();

void thread_entry(int core_id)
{
    while (!atomic_read(&g_wake_up[core_id]));
}


void init_bss()
{
    extern unsigned int _bss;
    extern unsigned int _ebss;
    unsigned int *dst;
    dst = &_bss;

    while(dst < &_ebss)
    {
        *dst++ = 0;
    }
}



void trap_init()
{
    w_stvec((uint64_t)trap_vector);
    w_sstatus(r_sstatus() | SSTATUS_SIE);
    w_sie(r_sie() | SIE_SEIE | SIE_SSIE);
    
    syslog_debug("init_trap","trap init done!");
}

void set_next_timeout()
{
    // There is a very strange bug,
    // if comment the `printf` line below
    // the timer will not work.

    // this bug seems to disappear automatically
    // printf("");
    sbi_set_timer(r_time() + INTERVAL);
}

void timer_init()
{
    initlock(&tickslock, "time");
    w_sie(r_sie() | SIE_STIE);
    set_next_timeout();
    syslog_debug("timer_init","timer init done!");
}

void init_kernel()
{
    syslog_print("\n[ Linux 0.11 ] starting linux 0.11 kernel...\n");
}

void entry(int core_id)
{
    syslog_print("\n---------------- ENTER LINUX KERNEL OUTPUT ZONE ------------------\n");
    syslog_test("entry", "finish register init, starting kernel......\r\n");

    if(core_id == 0)
    {
        core1_instance.callback = NULL;
        core1_instance.ctx = NULL;
        plic_init();
        core_init();
        trap_init();
        virtio_disk_init();
        supervisor_main();
    }
    else
    {
        //plic_init();
        //sysctl_enable_irq();
        //sysctl_disable_irq();
        thread_entry(core_id);

        if(core1_instance.callback == NULL)
        {
            while(1)
            {
                //asm volatile("wfi");
            }
        }
        else
        {
            core1_instance.callback(core1_instance.ctx);
        }
    }
}