#include "common.h"
#define __LIBRARY__
#include "unistd.h"
#include "time.h"
#include "linux/tty.h"
#include "linux/sched.h"
#include "stddef.h"
#include "stdarg.h"
#include "linux/fs.h"
#include <fcntl.h>



static inline _syscall0(int64_t,fork);
static inline _syscall0(int64_t,setup);
static inline _syscall0(int64_t,pause);
//static inline _syscall1(int64_t,write,char *,buf);
static inline _syscall3(int64_t,write,int,fd,const char *,buf,off_t,count);

static inline _syscall3(int64_t,open,const char *,filename,int,flag,int,mode);
static inline _syscall1(int64_t,dup,unsigned int,fildes);
static inline _syscall1(int64_t,exit,int,exit_code);
static inline _syscall3(int64_t,execve,const char *,file,char **,argv,char **,envp);
static inline _syscall3(int64_t,waitpid,pid_t,pid,uint *,stat_addr,int,options);
static inline _syscall0(int64_t,sync);
static inline _syscall0(pid_t,setsid);
static inline _syscall1(int64_t,close,int,fd);
static inline _syscall1(int64_t,debug,ulong,p);

void mem_init(ulong start_mem,ulong end_mem);
extern void blk_dev_init();
extern void chr_dev_init();
extern void rd_init(ulong mem_start,ulong length);
extern void rd_load();
extern int64_t kernel_mktime(struct tm *tm);

extern ulong _buffer_start;
extern ulong _buffer_end;

void machine_timer_test_interrupt_handler(void *ctx)
{
    syslog_info("machine_timer_test_interrupt_handler","");
}


static void time_init()
{
    struct tm time;

    time.tm_year = 2019;
    time.tm_mon = 11;
    time.tm_mday = 20;
    time.tm_hour = 22;
    time.tm_min = 51;
    time.tm_sec = 12;
    startup_time = kernel_mktime(&time);
}

static char * argv_rc[] = { "/bin/sh", NULL };
static char * envp_rc[] = { "HOME=/", NULL };

static char * argv[] = { "-/bin/sh",NULL };
static char * envp[] = { "HOME=/usr/root", NULL };

pid_t wait(int *wait_stat)
{
	return usersyscall_waitpid(-1,wait_stat,0);
}

void init()
{
    int pid,i;
    
    usersyscall_setup();
    usersyscall_setsid();
    usersyscall_open("/dev/tty0",O_RDWR,0);
    usersyscall_dup(0); 
    usersyscall_dup(0);  
    printf("%d buffers = %d bytes buffer space\r\n",NR_BUFFERS,NR_BUFFERS * BLOCK_SIZE);
    printf("Free mem: %d bytes\r\n",0x600000);

    if(!(pid = usersyscall_fork()))
    {
        usersyscall_close(0);

        if(usersyscall_open("/etc/rc",O_RDONLY,0))
        {
            usersyscall_exit(1); 
        }
        
        usersyscall_execve("/bin/sh",argv_rc,envp_rc);
        usersyscall_exit(2);
    }

    if(pid > 0)
    {
        while(pid != wait(&i));
    }

    while(1)
    {
        if((pid = usersyscall_fork()) < 0)
        {
            printf("Fork failed in init\r\n");
            continue;
        }

        if(!pid)
        {
            usersyscall_close(0);
            usersyscall_close(1);
            usersyscall_close(2);
            usersyscall_setsid();
            usersyscall_open("/dev/tty0",O_RDWR,0);
            usersyscall_dup(0);
            usersyscall_dup(0);
            ulong x = usersyscall_execve("/bin/sh",argv,envp);
            usersyscall_debug(errno);
            usersyscall_exit(x);
        }
        
        while(1)
        {
            if(pid == wait(&i))
            {
                break;
            }
        }

        printf("\r\nchild %d died with code %04x\r\n",pid,i);
        usersyscall_sync();
        while(1);
    }

    usersyscall_exit(0);
}

void user_main()
{
    //printf("[Linux 0.11] user_main: output from user mode successfully\r\n");
    //printf("[Linux 0.12] user_main: printf OK!\r\n");
    
    if(!usersyscall_fork())
    {
        init();
    }
    
    while(1)
    {
        usersyscall_pause();
    }
}

int64_t sys_setup()
{
    syslog_print("here!\r\n");
    //rd_load();
    //syslog_print("rd_load ok\r\n");
    mount_root();
    syslog_print("mount_root ok\r\n");
    return 0;
}

int supervisor_main()
{
    ulong i;

    /*
    pte_common_init((volatile pte_64model *)page_root_table,PAGE_ROOT_TABLE_NUM);

    pte_common_addr_to_ppn((volatile pte_64model *)&page_root_table[2],0x80000000UL);
    pte_common_set_accessibility((volatile pte_64model *)&page_root_table[2],pte_accessibility_all);
    pte_common_enable_entry((volatile pte_64model *)&page_root_table[2]);
    pte_common_enable_user((volatile pte_64model *)&page_root_table[2]);

    for(i = 3;i < PAGE_ROOT_TABLE_NUM;i++)
    {
        pte_common_set_accessibility((volatile pte_64model *)&page_root_table[i],pte_accessibility_pointer);
        pte_common_enable_entry((volatile pte_64model *)&page_root_table[i]);
    }

    pte_common_addr_to_ppn(&page_root_table[3],page_dir_table);
    pte_common_enable_user(&page_root_table[3]);

    pte_set_root_page_table((volatile pte_64model *)page_root_table);
    //pte_entry_sv39();
    syslog_dump_memory((void *)page_root_table,sizeof(page_root_table));
    */

    sysctl_disable_irq();

    syslog_print("sizeof(struct task_struct) = %d\r\n",sizeof(struct task_struct));
    ROOT_DEV = 0x0101;
    
    syslog_print("buffer_start = %p,buffer_end = %p\r\n",&_buffer_start,&_buffer_end);
    
    timer_init();
    syslog_print("timer_init ok\r\n");
    
    kinit();        // physical page allocator
    syslog_print("kinit ok\r\n");
    kvminit();      // create kernel page table
    syslog_print("kvminit ok\r\n");
    kvminithart();  // turn on paging
    // mem_init(0x80100000UL,0x80600000UL);
    syslog_print("kvminithart ok\r\n");

    rd_init(0x8007D000UL,0x5A000UL);
    syslog_print("rd_init ok\r\n");
    
    blk_dev_init();
    syslog_print("blk_dev_init ok\r\n");

    chr_dev_init();
    syslog_print("chr_dev_init ok\r\n");

    // TODO solve tty drivers in QEMU

    //tty_init();
    //syslog_print("tty_init ok\r\n");

    time_init();
    syslog_print("time_init ok\r\n");

    sched_init();
    syslog_print("sched_init ok\r\n");

    buffer_init(&_buffer_end);
    syslog_print("buffer_init ok\r\n");

    syslog_print("Congratulation! All devices in QEMU-Virt have been inited successfully!\r\n");

    sysctl_enable_irq();
    
    privilege_to_user(user_main);

    return 0;
}