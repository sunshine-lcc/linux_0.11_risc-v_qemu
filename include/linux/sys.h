#include "unistd.h"



extern int64_t sys_setup();
extern int64_t sys_exit();
extern int64_t sys_fork();
extern int64_t sys_read();
extern int64_t sys_write();
extern int64_t sys_open();
extern int64_t sys_close();
extern int64_t sys_waitpid();
extern int64_t sys_creat();
extern int64_t sys_link();
extern int64_t sys_unlink();
extern int64_t sys_execve();
extern int64_t sys_chdir();
extern int64_t sys_time();
extern int64_t sys_mknod();
extern int64_t sys_chmod();
extern int64_t sys_chown();
extern int64_t sys_break();
extern int64_t sys_stat();
extern int64_t sys_lseek();
extern int64_t sys_getpid();
extern int64_t sys_mount();
extern int64_t sys_umount();
extern int64_t sys_setuid();
extern int64_t sys_getuid();
extern int64_t sys_stime();
extern int64_t sys_ptrace();
extern int64_t sys_alarm();
extern int64_t sys_fstat();
extern int64_t sys_pause();
extern int64_t sys_utime();
extern int64_t sys_stty();
extern int64_t sys_gtty();
extern int64_t sys_access();
extern int64_t sys_nice();
extern int64_t sys_ftime();
extern int64_t sys_sync();
extern int64_t sys_kill();
extern int64_t sys_rename();
extern int64_t sys_mkdir();
extern int64_t sys_rmdir();
extern int64_t sys_dup();
extern int64_t sys_pipe();
extern int64_t sys_times();
extern int64_t sys_prof();
extern int64_t sys_brk();
extern int64_t sys_setgid();
extern int64_t sys_getgid();
extern int64_t sys_signal(int signum,int64_t handler,int64_t restorer);
extern int64_t sys_geteuid();
extern int64_t sys_getegid();
extern int64_t sys_acct();
extern int64_t sys_phys();
extern int64_t sys_lock();
extern int64_t sys_ioctl();
extern int64_t sys_fcntl();
extern int64_t sys_mpx();
extern int64_t sys_setpgid();
extern int64_t sys_ulimit();
extern int64_t sys_uname();
extern int64_t sys_umask();
extern int64_t sys_chroot();
extern int64_t sys_ustat();
extern int64_t sys_dup2();
extern int64_t sys_getppid();
extern int64_t sys_getpgrp();
extern int64_t sys_setsid();
extern int64_t sys_sigaction(int signum,const struct sigaction *action,struct sigaction *oldaction);
extern int64_t sys_sgetmask();
extern int64_t sys_ssetmask(int newmask);
extern int64_t sys_setreuid();
extern int64_t sys_setregid();
extern int64_t sys_debug(int p);

fn_ptr sys_call_table[] = {
    [__NR_setup] sys_setup,
    [__NR_fork] sys_fork,
    [__NR_waitpid] sys_waitpid,
    [__NR_creat] sys_creat,
    [__NR_execve] sys_execve,
    [__NR_mknod] sys_mknod,
    [__NR_chmod] sys_chmod,
    [__NR_chown] sys_chown,
    [__NR_break] sys_break,
    [__NR_mount] sys_mount,
    [__NR_umount] sys_umount,
    [__NR_setuid] sys_setuid,
    [__NR_stime] sys_stime,
    [__NR_ptrace] sys_ptrace,
    [__NR_alarm] sys_alarm,
    [__NR_pause] sys_pause,
    [__NR_utime] sys_utime,
    [__NR_stty] sys_stty,
    [__NR_gtty] sys_gtty,
    [__NR_nice] sys_nice,
    [__NR_ftime] sys_ftime,
    [__NR_sync] sys_sync,
    [__NR_dup] sys_dup,
    [__NR_rename] sys_rename,
    [__NR_fcntl] sys_fcntl,
    [__NR_rmdir] sys_rmdir,
    [__NR_pipe] sys_pipe,
    [__NR_prof] sys_prof,
    [__NR_setgid] sys_setgid,
    [__NR_signal] sys_signal,
    [__NR_acct] sys_acct,
    [__NR_phys] sys_phys,
    [__NR_lock] sys_lock,
    [__NR_ioctl] sys_ioctl,
    [__NR_mpx] sys_mpx,
    [__NR_setpgid] sys_setpgid,
    [__NR_ulimit] sys_ulimit,
    [__NR_umask] sys_umask,
    [__NR_chroot] sys_chroot,
    [__NR_ustat] sys_ustat,
    [__NR_dup2] sys_dup2,
    [__NR_getppid] sys_getppid,
    [__NR_getpgrp] sys_getpgrp,
    [__NR_setsid] sys_setsid,
    [__NR_sigaction] sys_sigaction,
    [__NR_sgetmask] sys_sgetmask,
    [__NR_ssetmask] sys_ssetmask,
    [__NR_chdir] sys_chdir,
    [__NR_setreuid] sys_setreuid,
    [__NR_setregid] sys_setregid,
    [__NR_debug] sys_debug,
    [__NR_close] sys_close,
    [__NR_lseek] sys_lseek,
    [__NR_read] sys_read,
    [__NR_write] sys_write,
    [__NR_fstat] sys_fstat,
    [__NR_exit] sys_exit,
    [__NR_kill] sys_kill,
    [__NR_times] sys_times,
    [__NR_uname] sys_uname,
    [__NR_getpid] sys_getpid,
    [__NR_getuid] sys_getuid,
    [__NR_geteuid] sys_geteuid,
    [__NR_getgid] sys_getgid,
    [__NR_getegid] sys_getegid,
    [__NR_brk] sys_brk,
    [__NR_open] sys_open,
    [__NR_link] sys_link,
    [__NR_unlink] sys_unlink,
    [__NR_mkdir] sys_mkdir,
    [__NR_access] sys_access,
    [__NR_stat] sys_stat,
    [__NR_time] sys_time,
};
