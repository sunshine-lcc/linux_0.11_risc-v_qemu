#include "common.h"
#include "linux/kernel.h"
//#include <linux/sched.h>

//void sys_sync(void);	/* it's really int */

volatile void panic(const char * s)
{
	printk("Kernel panic: %s\n\r",s);
	for(;;);
}